package com.oussema.carrental.repository;

import com.oussema.carrental.model.Car;
import com.oussema.carrental.model.CarDetail;
import com.oussema.carrental.model.RentalBody;
import com.oussema.carrental.model.RentalResponse;
import com.oussema.carrental.networking.APIClient;
import com.oussema.carrental.networking.APIService;
import com.oussema.carrental.utils.Constants;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CarRepository {

    private APIService service;

    public CarRepository() {
        service = APIClient.getClient();
    }

    public Single<List<Car>> getCars() {
        return service.getAllCars()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<CarDetail> getCars(int id) {
        return service.getCarById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<RentalResponse> sendRentalRequest(int id) {
        return service.addRental(Constants.POST_URL, new RentalBody(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
