package com.oussema.carrental.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarDetail {
    @SerializedName("carId")
    @Expose
    private int carId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isClean")
    @Expose
    private boolean isClean;
    @SerializedName("isDamaged")
    @Expose
    private boolean isDamaged;
    @SerializedName("licencePlate")
    @Expose
    private String licencePlate;
    @SerializedName("fuelLevel")
    @Expose
    private int fuelLevel;
    @SerializedName("vehicleStateId")
    @Expose
    private int vehicleStateId;
    @SerializedName("hardwareId")
    @Expose
    private String hardwareId;
    @SerializedName("vehicleTypeId")
    @Expose
    private int vehicleTypeId;
    @SerializedName("pricingTime")
    @Expose
    private String pricingTime;
    @SerializedName("pricingParking")
    @Expose
    private String pricingParking;
    @SerializedName("isActivatedByHardware")
    @Expose
    private boolean isActivatedByHardware;
    @SerializedName("locationId")
    @Expose
    private int locationId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("lat")
    @Expose
    private float lat;
    @SerializedName("lon")
    @Expose
    private float lon;
    @SerializedName("reservationState")
    @Expose
    private int reservationState;
    @SerializedName("damageDescription")
    @Expose
    private String damageDescription;
    @SerializedName("vehicleTypeImageUrl")
    @Expose
    private String vehicleTypeImageUrl;

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClean() {
        return isClean;
    }

    public void setClean(boolean clean) {
        isClean = clean;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public int getVehicleStateId() {
        return vehicleStateId;
    }

    public void setVehicleStateId(int vehicleStateId) {
        this.vehicleStateId = vehicleStateId;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }

    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getPricingTime() {
        return pricingTime;
    }

    public void setPricingTime(String pricingTime) {
        this.pricingTime = pricingTime;
    }

    public String getPricingParking() {
        return pricingParking;
    }

    public void setPricingParking(String pricingParking) {
        this.pricingParking = pricingParking;
    }

    public boolean isActivatedByHardware() {
        return isActivatedByHardware;
    }

    public void setActivatedByHardware(boolean activatedByHardware) {
        isActivatedByHardware = activatedByHardware;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public int getReservationState() {
        return reservationState;
    }

    public void setReservationState(int reservationState) {
        this.reservationState = reservationState;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

    public void setVehicleTypeImageUrl(String vehicleTypeImageUrl) {
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }

    public ArrayList<Detail> toDetailsArray() {
        ArrayList<Detail> details = new ArrayList<>();
        details.add(new Detail("Clean", String.valueOf(isClean)));
        details.add(new Detail("Damaged", String.valueOf(isDamaged)));
        details.add(new Detail("fuel Level", String.valueOf(fuelLevel)));
        details.add(new Detail("vehicle State", String.valueOf(vehicleStateId)));
        details.add(new Detail("vehicle Type", String.valueOf(vehicleTypeId)));
        details.add(new Detail("pricing Time", String.valueOf(pricingTime)));
        details.add(new Detail("pricing Parking", String.valueOf(pricingParking)));
        details.add(new Detail("Activated By Hardware", String.valueOf(isActivatedByHardware)));
        details.add(new Detail("location", String.valueOf(locationId)));
        details.add(new Detail("reservation State", String.valueOf(reservationState)));
        details.add(new Detail("damage Description", String.valueOf(damageDescription)));
        return details;
    }
}
