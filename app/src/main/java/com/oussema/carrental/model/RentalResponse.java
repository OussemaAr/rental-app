package com.oussema.carrental.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RentalResponse {
    @SerializedName("reservationId")
    @Expose
    private int reservationId;
    @SerializedName("carId")
    @Expose
    private int carId;
    @SerializedName("cost")
    @Expose
    private int cost;
    @SerializedName("drivenDistance")
    @Expose
    private int drivenDistance;
    @SerializedName("licencePlate")
    @Expose
    private String licencePlate;
    @SerializedName("startAddress")
    @Expose
    private String startAddress;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("isParkModeEnabled")
    @Expose
    private boolean isParkModeEnabled;
    @SerializedName("damageDescription")
    @Expose
    private String damageDescription;
    @SerializedName("fuelCardPin")
    @Expose
    private String fuelCardPin;
    @SerializedName("endTime")
    @Expose
    private int endTime;
    @SerializedName("startTime")
    @Expose
    private int startTime;

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getDrivenDistance() {
        return drivenDistance;
    }

    public void setDrivenDistance(int drivenDistance) {
        this.drivenDistance = drivenDistance;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isParkModeEnabled() {
        return isParkModeEnabled;
    }

    public void setParkModeEnabled(boolean parkModeEnabled) {
        isParkModeEnabled = parkModeEnabled;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getFuelCardPin() {
        return fuelCardPin;
    }

    public void setFuelCardPin(String fuelCardPin) {
        this.fuelCardPin = fuelCardPin;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }
}
