package com.oussema.carrental.model;

public class RentalBody {

    private int carId;

    public RentalBody(int carId) {
        this.carId = carId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }
}
