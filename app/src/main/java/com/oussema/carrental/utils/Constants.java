package com.oussema.carrental.utils;

public class Constants {

    public static final String BASE_URL = "https://s3.eu-central-1.amazonaws.com/";
    public static final String POST_URL = "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental";
}
