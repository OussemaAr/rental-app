package com.oussema.carrental.utils;

public interface DismissCallback {
    void onDismissed();
}
