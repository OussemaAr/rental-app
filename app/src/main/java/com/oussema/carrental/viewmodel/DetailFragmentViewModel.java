package com.oussema.carrental.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.oussema.carrental.model.CarDetail;
import com.oussema.carrental.model.RentalResponse;
import com.oussema.carrental.repository.CarRepository;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class DetailFragmentViewModel extends AndroidViewModel {

    private CarRepository mRepository;
    private MutableLiveData<CarDetail> carMutableLiveData = new MutableLiveData<>();
    private LiveData<CarDetail> carLiveData = carMutableLiveData;
    private MutableLiveData<RentalResponse> rentalMutableLiveData = new MutableLiveData<>();
    private LiveData<RentalResponse> rentalLiveData = rentalMutableLiveData;
    private CompositeDisposable disposable = new CompositeDisposable();

    public DetailFragmentViewModel(@NonNull Application application) {
        super(application);
        mRepository = new CarRepository();
    }

    public LiveData<CarDetail> getCarDetail(int id) {
        DisposableSingleObserver<CarDetail> disposableSingleObserver = mRepository.getCars(id)
                .subscribeWith(new DisposableSingleObserver<CarDetail>() {
                    @Override
                    public void onSuccess(CarDetail car) {
                        carMutableLiveData.postValue(car);
                    }

                    @Override
                    public void onError(Throwable e) {
                        carMutableLiveData.postValue(null);
                    }
                });
        disposable.add(disposableSingleObserver);
        return carLiveData;
    }

    public LiveData<RentalResponse> sendRentingRequest(int id) {
        DisposableSingleObserver<RentalResponse> disposableSingleObserver = mRepository.sendRentalRequest(id)
                .subscribeWith(new DisposableSingleObserver<RentalResponse>() {
                    @Override
                    public void onSuccess(RentalResponse rentalResponse) {
                        rentalMutableLiveData.postValue(rentalResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        rentalMutableLiveData.postValue(null);
                    }
                });
        disposable.add(disposableSingleObserver);
        return rentalLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!disposable.isDisposed())
            disposable.dispose();
    }
}
