package com.oussema.carrental.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.oussema.carrental.model.Car;
import com.oussema.carrental.repository.CarRepository;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class MapViewModel extends AndroidViewModel {

    private CarRepository mRepository;
    private MutableLiveData<List<Car>> carMutableLiveData = new MutableLiveData<>();
    private LiveData<List<Car>> carLiveData = carMutableLiveData;
    private CompositeDisposable disposable = new CompositeDisposable();

    public MapViewModel(@NonNull Application application) {
        super(application);
        mRepository = new CarRepository();
    }

    public LiveData<List<Car>> getAllCars() {
        DisposableSingleObserver<List<Car>> disposableSingleObserver = mRepository.getCars()
                .subscribeWith(new DisposableSingleObserver<List<Car>>() {
                    @Override
                    public void onSuccess(List<Car> car) {
                        carMutableLiveData.postValue(car);
                    }

                    @Override
                    public void onError(Throwable e) {
                        carMutableLiveData.postValue(null);
                    }
                });
        disposable.add(disposableSingleObserver);
        return carLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!disposable.isDisposed())
            disposable.dispose();
    }
}
