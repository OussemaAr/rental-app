package com.oussema.carrental.networking;

import com.oussema.carrental.model.Car;
import com.oussema.carrental.model.CarDetail;
import com.oussema.carrental.model.RentalBody;
import com.oussema.carrental.model.RentalResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface APIService {

    @POST
    @Headers("Authorization: Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa")
    Single<RentalResponse> addRental(@Url String url, @Body RentalBody body);

    @GET("wunderfleet-recruiting-dev/cars.json")
    Single<List<Car>> getAllCars();

    @GET("wunderfleet-recruiting-dev/cars/{id}")
    Single<CarDetail> getCarById(@Path("id") int id);
}
