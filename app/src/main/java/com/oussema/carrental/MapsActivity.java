package com.oussema.carrental;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.oussema.carrental.databinding.ActivityMapsBinding;
import com.oussema.carrental.fragment.DetailDialogFragment;
import com.oussema.carrental.model.Car;
import com.oussema.carrental.utils.DismissCallback;
import com.oussema.carrental.viewmodel.MapViewModel;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, DismissCallback {

    private GoogleMap mMap;
    private ArrayList<Marker> markers = new ArrayList<>();
    private int selectedMarker = -1;
    private Marker mPosition;
    private final int REQUEST_CODE = 2000;
    private ActivityMapsBinding binding;
    private final int LOCATION_REQUEST = 2020;

    private LocationCallback callback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (mPosition != null)
                mPosition.remove();
            drawMarkerMe(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
        }
    };

    private void drawMarkerMe(double latitude, double longitude) {
        LatLng loc = new LatLng(latitude, longitude);
        mPosition = mMap.addMarker(new MarkerOptions().position(loc).title("ME"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        requestLocation();
    }

    private void requestLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            }
        } else {
            checkGPSEnabled();
            fusedLocationClient.requestLocationUpdates(locationRequest, callback, null);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        MapViewModel mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
        mapViewModel.getAllCars().observe(this, cars -> {
            if (cars != null) {
                for (Car car : cars) {
                    Marker marker = mMap.addMarker(new MarkerOptions().title(car.getTitle()).position(new LatLng(car.getLat(), car.getLon())));
                    marker.setTag(car.getCarId());
                    markers.add(marker);
                }
            } else {
                new MaterialAlertDialogBuilder(MapsActivity.this)
                        .setTitle("Error")
                        .setMessage("Check your internet connection")
                        .setCancelable(false)
                        .setNeutralButton("OK", (dialog, which) -> dialog.dismiss())
                        .create()
                        .show();
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTag() != null) {
            int id = Integer.parseInt(marker.getTag().toString());
            if (id == selectedMarker) {
                DetailDialogFragment detailDialogFragment = DetailDialogFragment.newInstance(id);
                detailDialogFragment.setDismissCallback(this);
                detailDialogFragment.show(getSupportFragmentManager(), "dialog");
            } else {
                selectedMarker = id;
                removeMarkers(id);
            }
        }
        return false;
    }

    private void removeMarkers(int i) {
        for (Marker marker : markers) {
            assert marker.getTag() != null;
            if (Integer.parseInt(marker.getTag().toString()) != i) {
                marker.setVisible(false);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Snackbar.make(binding.getRoot(), "You need to activate your GPS", Snackbar.LENGTH_LONG).show();
            } else {
                checkGPSEnabled();
                requestLocation();
            }
        }
    }

    private void checkGPSEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new MaterialAlertDialogBuilder(MapsActivity.this)
                    .setTitle("Enable your GPS")
                    .setMessage("Please Enable your GPS")
                    .setCancelable(false)
                    .setNeutralButton("OK", (dialog, which) -> startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_REQUEST))
                    .create()
                    .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_REQUEST) {
            requestLocation();
        } else {
            Snackbar snackbar = Snackbar.make(binding.getRoot(), "You need to activate your GPS", Snackbar.LENGTH_LONG);
            snackbar.setAction("Grant", v -> checkGPSEnabled());
            snackbar.show();
        }
    }

    @Override
    public void onDismissed() {
        selectedMarker = -1;
        for (Marker marker : markers) {
            marker.setVisible(true);
        }
    }

    @Override
    public void onBackPressed() {
        onDismissed();
    }
}