package com.oussema.carrental.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.jakewharton.rxbinding3.view.RxView;
import com.oussema.carrental.adapter.DetailsAdapter;
import com.oussema.carrental.databinding.FragmentDetailDialogBinding;
import com.oussema.carrental.model.CarDetail;
import com.oussema.carrental.utils.DismissCallback;
import com.oussema.carrental.viewmodel.DetailFragmentViewModel;

import org.jetbrains.annotations.NotNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class DetailDialogFragment extends BottomSheetDialogFragment {

    private static final String ITEM_ID = "id";
    private FragmentDetailDialogBinding binding;
    private CompositeDisposable disposable = new CompositeDisposable();
    private DetailFragmentViewModel viewModel;
    private DismissCallback dismissCallback;

    public static DetailDialogFragment newInstance(int id) {
        final DetailDialogFragment fragment = new DetailDialogFragment();
        final Bundle args = new Bundle();
        args.putInt(ITEM_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentDetailDialogBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(DetailFragmentViewModel.class);
        assert getArguments() != null;
        viewModel.getCarDetail(getArguments().getInt(ITEM_ID)).observe(getViewLifecycleOwner(), car -> {
            if (car != null)
                UpdateUI(car);
            else {
                displayDialog("Problem accrued", "Please try again later");
                dismiss();
            }
        });
    }

    private void UpdateUI(final CarDetail car) {
        Glide.with(requireContext())
                .load(car.getVehicleTypeImageUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(binding.image);

        binding.title.setText(String.format("%s (%s)", car.getTitle(), car.getLicencePlate()));
        binding.address.setText(String.format("%s, %s , %s", car.getAddress(), car.getZipCode(), car.getCity()));
        binding.hardwareId.setText(car.getHardwareId());

        binding.recycler.setLayoutManager(new GridLayoutManager(requireContext(), 3));
        DetailsAdapter adapter = new DetailsAdapter(car.toDetailsArray());
        binding.recycler.setAdapter(adapter);
        Disposable subscribe = RxView.clicks(binding.quickRent)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(unit -> {
                    binding.quickRent.setVisibility(View.GONE);
                    binding.loading.setVisibility(View.VISIBLE);
                    viewModel.sendRentingRequest(car.getCarId()).
                            observe(getViewLifecycleOwner(), rentalResponse -> {
                                dismiss();
                                if (rentalResponse != null)
                                    displayDialog("Rental Added", "You did it");
                                else
                                    displayDialog("Problem accrued", "Please try again later");

                            });
                });
        disposable.add(subscribe);
        binding.progress.setVisibility(View.GONE);
        binding.container.setVisibility(View.VISIBLE);
    }

    private void displayDialog(String title, String message) {
        new MaterialAlertDialogBuilder(requireContext())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("OK", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    public void setDismissCallback(DismissCallback dismissCallback) {
        this.dismissCallback = dismissCallback;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        dismissCallback.onDismissed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!disposable.isDisposed())
            disposable.dispose();
    }
}