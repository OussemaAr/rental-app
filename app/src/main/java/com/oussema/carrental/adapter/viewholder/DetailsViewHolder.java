package com.oussema.carrental.adapter.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.oussema.carrental.databinding.ItemDetailBinding;

public class DetailsViewHolder extends RecyclerView.ViewHolder {


    private ItemDetailBinding binding;

    public DetailsViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemDetailBinding.bind(itemView);
    }

    public void bind(String key, String value) {
        binding.key.setText(key);
        binding.value.setText(value);
    }
}
