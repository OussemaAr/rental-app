package com.oussema.carrental.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.oussema.carrental.adapter.viewholder.DetailsViewHolder;
import com.oussema.carrental.databinding.ItemDetailBinding;
import com.oussema.carrental.model.Detail;

import java.util.ArrayList;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsViewHolder> {

    private ArrayList<Detail> data;

    public DetailsAdapter(ArrayList<Detail> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public DetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDetailBinding inflate = ItemDetailBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new DetailsViewHolder(inflate.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsViewHolder holder, int position) {
        Detail item = data.get(position);
        holder.bind(item.getKey(), item.getValue());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
